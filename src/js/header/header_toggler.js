var nav = document.querySelector('.navigation');
var navToggle = nav.querySelector('.navigation__toggle-btn');
//var navBackground = nav.querySelector('.navigation-background');

navToggle.addEventListener('click', function () {
    var body = document.querySelector('body');
    if (nav.classList.contains("navigation--opened")) {
        nav.classList.remove("navigation--opened");
        nav.classList.add("navigation--closed");
        body.classList.remove("nav-opened");
    } else {
        nav.classList.remove("navigation--closed");
        nav.classList.add("navigation--opened");
        body.classList.add("nav-opened");
    }
});
